package com.android.wildfire.network;

import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.android.wildfire.util.AppConstants;
import com.android.wildfire.util.AppUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class HttpAsyncTask extends AsyncTask<String, String, String> {
	
	private static final String TAG = HttpAsyncTask.class.getSimpleName();
	
	private Context parentCtx;
	private JSONObject mJsonObject;
	private final ProgressDialog dialog;
	private ServiceListener listener = null;
	
	private boolean mDisablePD = true ;
	
	public HttpAsyncTask(Context context, JSONObject jsonObject, ServiceListener serviceListener){
		
		this.parentCtx = context ;
		this.mJsonObject = jsonObject ;
		this.listener = serviceListener ;
		this.dialog = new ProgressDialog(context);
		
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
		if(mDisablePD){
			dialog.setMessage(AppConstants.CONNECTING);
			dialog.setCancelable(false);
			dialog.show();
		}
		
	}

	
	
	@Override
	protected String doInBackground(String... params) {
		
		if (AppUtil.isInternetAvailable(parentCtx)) {
			
			byte[] result = null;
			String str = "";
			HttpClient httpclient = new DefaultHttpClient();
			HttpParams myParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
			HttpConnectionParams.setSoTimeout(myParams, 10000);

			JSONObject json = new JSONObject();  

			try {

				HttpPost httppost = new HttpPost(params[0]);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				
				System.out.println("url------->"+params[0]);
				
				json = mJsonObject;
				
				Log.i(TAG, "+++++++"+json.toString());
				
				StringEntity se = new StringEntity(json.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				httppost.setEntity(se);
				
				HttpResponse response = httpclient.execute(httppost);
				if (response.getEntity().getContentLength()==0) {
					 return AppConstants.SERVER_ERROR;
				}else{
					 result = EntityUtils.toByteArray(response.getEntity());
                     str = new String(result, "UTF-8");
                     Log.i(TAG, "return vaule-->\n"+str);
                     return str;
				}
				
                
			
			} catch (UnsupportedEncodingException e) {
 
				e.printStackTrace();
				return AppConstants.SERVER_ERROR;

			}	catch (ConnectTimeoutException cte) {
                cte.printStackTrace();
                return AppConstants.SERVER_ERROR;
            }
			catch (SocketTimeoutException ste) {
			    ste.printStackTrace();
                return AppConstants.SERVER_ERROR;
                
            } catch (Exception e) {

				e.printStackTrace();
				return AppConstants.SERVER_ERROR;

			}

		} else {
			
			return AppConstants.SERVER_ERROR;

		}
	}
	
	
	
	@Override
	protected void onPostExecute(String result) {
		
		if(dialog!=null && dialog.isShowing()){
			dialog.dismiss();
		}
		 
	  listener.onServiceComplete(result);
	 
	}

}
