package com.android.wildfire;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.android.wildfire.ui.LoginActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	 private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));
	    private Button hiddenExitButton, startQuizButton;

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
	        setContentView(R.layout.splash_screen);

	        // every time someone enters the kiosk mode, set the flag true
	        PrefUtils.setKioskModeActive(true, getApplicationContext());

	    /*    hiddenExitButton = (Button) findViewById(R.id.hiddenExitButton);
	        hiddenExitButton.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View v) {
	                // Break out!
	                PrefUtils.setKioskModeActive(false, getApplicationContext());
	                Toast.makeText(getApplicationContext(),"You can leave the app now!", Toast.LENGTH_SHORT).show();
	            }
	        });*/
	        
	        startQuizButton = (Button)findViewById(R.id.start_quiz_btn);
	        startQuizButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					 Intent startIntent = new Intent(MainActivity.this,LoginActivity.class);
					 startActivity(startIntent);
				}
			});
	        
	    }

	    @Override
	    public void onWindowFocusChanged(boolean hasFocus) {
	        super.onWindowFocusChanged(hasFocus);
	        if(!hasFocus) {
	            // Close every kind of system dialog
	        	Toast.makeText(getApplicationContext(),"Close every kind of system dialog!", Toast.LENGTH_SHORT).show();
	            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
	            sendBroadcast(closeDialog);
	        }
	    }

	    @Override
	    public void onBackPressed() {
	        // nothing to do here
	        // � really
	    }

	    @Override
	    public boolean dispatchKeyEvent(KeyEvent event) {
	        if (blockedKeys.contains(event.getKeyCode())) {
	            return true;
	        } else {
	            return super.dispatchKeyEvent(event);
	        }
	    }
}
